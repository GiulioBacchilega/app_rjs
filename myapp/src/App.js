import Todo from "./components/Todo";

function App() {
  return (
    <div>
      <h1>Learn React</h1>
      <Todo text="Learn React"/>
      <Todo text="Master React"/>
      <Todo text="Explore full React course"/>
    </div>
  );
}

export default App;
