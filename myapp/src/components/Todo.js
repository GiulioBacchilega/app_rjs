import {useState} from 'react';
import Modal from './Modal';
import Backdrop from './Backdrop';

function Todo(props) {

    const [modalIsOpen, setModalIsOpen] = useState(false);      //stato del componente, che può essere storicizzato in un array, il quale è composto da una variabile e da una funzione per modificare il valore 

    function deleteHandler() {
     //   console.log(props.text)
        setModalIsOpen(true);
    }
    
    function closeModalHandler(){
        setModalIsOpen(false);
    }

    return (
        <div className="card">
            <div>
                <h2>{props.text}</h2>
                <div className="actions">
                    <span>A Span</span>
                    <button className="btn" onClick={deleteHandler}>Delete</button>
                </div>
                 {modalIsOpen && <Modal onCancel={closeModalHandler} onConfirm={closeModalHandler}/>/* --->  {modalIsOpen ? <Modal/> : null}  */}
                 {modalIsOpen && <Backdrop onClick={closeModalHandler}/>}
            </div>
        </div>
    );
}

export default Todo;