function Backdrop(props){
     return <div className="backdrop" onClick={props.onClick}/>      //richiamo l'handler tramite props
}

export default Backdrop;