import classes from './Card.module.css'

function Card(card) {
    return (
        <div className={classes.card}>
            {card.children}
        </div>
    )
}

export default Card;