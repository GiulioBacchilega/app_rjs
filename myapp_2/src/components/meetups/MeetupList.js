import MeetupItem from  './MeetupItem'
import classes from './MeetupList.module.css'


function MeetupList(list)
{
    return (
        <ul className={classes.list}>
            {list.meetups.map(meetup => 
                <MeetupItem 
                    key={meetup.id} 
                    id={meetup.id} 
                    image={meetup.image} 
                    title={meetup.address}
                    address={meetup.address}
                    description={meetup.description}/>)}
        </ul>
    )
}

export default MeetupList;